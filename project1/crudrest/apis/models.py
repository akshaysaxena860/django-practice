from django.db import models

# Create your models here.
class StudentModel(models.Model):
    name=models.CharField(max_length=20)
    roll=models.IntegerField()
    city=models.CharField(max_length=30)


    def __str__(self) -> str:
        return self.name+" "+str(self.roll)+" "+self.city
