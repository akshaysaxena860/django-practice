from django.urls import path
from .views import studentcrud, home
urlpatterns = [
    path("", home),
    path("getdata", studentcrud)
]
