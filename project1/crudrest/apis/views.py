from http.client import HTTPResponse
from django.shortcuts import render
from django.http import request,HttpResponse
import io
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from .serializers import StudentSerializers

from .models import StudentModel
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

def home(request):
    return HttpResponse("Hello")

@csrf_exempt
def studentcrud(request):
    # Gettting Student Data
    if request.method=="GET":
        json_data=request.body
        stream=io.BytesIO(json_data)
        stud_obj=JSONParser().parse(stream)
        id=stud_obj.get('id',None)
        if id is not None:
            stud=StudentModel.objects.get(id=id)
            print("Data ",stud)
            serializer=StudentSerializers(stud)
            json_data=JSONRenderer().render(serializer.data)
            return HttpResponse(json_data,content_type='application/json')
        stud=StudentModel.objects.all()
        serializer=StudentSerializers(stud,many=True)
        json_data=JSONRenderer().render(serializer.data)
        return HttpResponse(json_data,content_type='application/json')
    # Creating Student Data
    if request.method=="POST":
        json_data=request.body
        print("DATA",json_data)
        stream=io.BytesIO(json_data)
        print(stream)
        stud_data=JSONParser().parse(stream)
        print(stud_data)
        serialize_stud=StudentSerializers(data=stud_data)
        print(serialize_stud)
        if serialize_stud.is_valid():
            serialize_stud.save()
            json_data=JSONRenderer().render({"msg":"Data Created Successfully"})
            return HttpResponse(json_data,content_type="application/json")
        json_data=JSONRenderer().render(serialize_stud.error_messages)
        return HttpResponse(json_data,content_type="application/json")  

    # Update Data
    if request.method=="PUT":
        json_data=request.body
        stream=io.BytesIO(json_data)
        stud_data=JSONParser().parse(stream)
        print(stud_data)
        id=stud_data.get('id')
        print("ID ",id)
        stu=StudentModel.objects.get(id=id)
        serializer=StudentSerializers(stu,data=stud_data)
        if serializer.is_valid():
            serializer.save()
            res={'msg':'Data Updated Successfully'}
            json_data=JSONRenderer().render(res)
            return HttpResponse(json_data,content_type="application/json")
        
        json_data=JSONRenderer().render(serializer.error_messages)
        return HttpResponse(json_data,content_type="application/json")
    # Delete Data
    if request.method=="DELETE":
        json_data=request.body
        stream=io.BytesIO(json_data)
        stud_data=JSONParser().parse(stream)
        id=stud_data.get('id')
        stu=StudentModel.objects.get(id=id)
        stu.delete()
        res={'msg':'Data Deleted Successfully'}
        json_data=JSONRenderer().render(res)
        return HttpResponse(json_data,content_type="application/json")

