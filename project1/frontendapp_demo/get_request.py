import requests
import json
url="http://127.0.0.1:8000/getdata"
data={
    "id":1,
    "name":"Akshat Saxena",
    "roll":11,
    "city":"Ujjain"
}

def getData(id=None):
    dt={
        "id":id
    }
    json_data=json.dumps(dt)
    r=requests.get(url=url,data=json_data)
    dt=r.json()
    print(dt)

def postData():
    json_data=json.dumps(data)
    r=requests.post(url=url,data=json_data)
    d=r.json()
    print(d)

def updateData():
    json_data=json.dumps(data)
    r=requests.put(url=url,data=json_data)
    d=r.json()
    print(d)

def deleteData():
    data={
        "id":1
    }
    json_data=json.dumps(data)
    r=requests.delete(url=url,data=json_data)
    d=r.json()
    print(d)

# postData()
# updateData()
# getData(1)
deleteData()
